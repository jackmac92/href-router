# href-router

A linux program to route links to the configured chrome profile

## How to use this?

1. Install the href-router binary (via cargo, or downloading the appropriate release)
2. Run `href-router install`
3. Update configuration at `$XDG_CONFIG_HOME/href-router/href-router.toml`
4. Make sure you didn't bork the config `href-router validate`
5. Good to go!\*

\*actually setting the default app on linux doesn't really have a "source of truth", so Good to go! is a bit of WIP (aka sometimes an app will manage to bypass href-router)

## Development status

This works for me pretty well as is, I'd be happy to take a look at your PR, but probably am not interested in adding your feature request (although I'll try to answer quickly if you would like to fork and have questions!)

## Alternatives

- [Choosy (OSX)](https://www.choosyosx.com/)
