use clap::{crate_authors, crate_description, crate_name, crate_version, App, AppSettings, Arg};

fn is_href(v: String) -> Result<(), String> {
    if v.contains('.') {
        return Ok(());
    }
    Err(String::from("The link does not look like a link"))
}

pub fn new() -> App<'static, 'static> {
    App::new(crate_name!())
        .version(crate_version!())
        .about(crate_description!())
        .author(crate_authors!())
        .setting(AppSettings::ArgRequiredElseHelp)
        .setting(AppSettings::ColorAuto)
        .arg(
            Arg::with_name("link")
                .help("link to open")
                .index(1)
                .validator(is_href),
        )
        .arg(
            Arg::with_name("personal")
                .short("P")
                .long("personal")
                .help("use non work browser during work hours"),
        )
        .subcommand(
            App::new("open")
                .about("Handle opening default browser without a link")
                .arg(Arg::with_name("link").help("link to open")),
        )
        .subcommand(
            App::new("check")
                .about("Output which browser profile would be used for a link")
                .arg(
                    Arg::with_name("link")
                        .help("link to check")
                        .validator(is_href),
                ),
        )
        .subcommand(App::new("install").about("Install/setup href-router"))
        .subcommand(App::new("validate").about("Check user config is valid"))
}
