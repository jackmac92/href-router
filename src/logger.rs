extern crate dirs;
use log::LevelFilter;
use log4rs::append::console::ConsoleAppender;
use log4rs::append::file::FileAppender;
use log4rs::config::{Appender, Config, Root};
use log4rs::encode::pattern::PatternEncoder;

pub fn create(should_log_to_stdout: bool) {
    let mut logfilepath = match dirs::home_dir() {
        Some(d) => d,
        None => panic!("Failed to find home dir"),
    };

    logfilepath.push(".local/log/href-router/activity.log");

    let stdout = ConsoleAppender::builder().build();

    let logfile = FileAppender::builder()
        .encoder(Box::new(PatternEncoder::new("{d} - {m}{n}")))
        .build(logfilepath)
        .unwrap();

    let config = if should_log_to_stdout {
        Config::builder()
            .appender(Appender::builder().build("stdout", Box::new(stdout)))
            .appender(Appender::builder().build("logfile", Box::new(logfile)))
            .build(
                Root::builder()
                    .appender("stdout")
                    .appender("logfile")
                    .build(LevelFilter::Debug),
            )
            .unwrap()
    } else {
        Config::builder()
            .appender(Appender::builder().build("logfile", Box::new(logfile)))
            .build(
                Root::builder()
                    .appender("logfile")
                    .build(LevelFilter::Debug),
            )
            .unwrap()
    };

    let _handle = log4rs::init_config(config).unwrap();
}
