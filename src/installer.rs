extern crate dirs;
use indoc::indoc;
use std::fs;
use std::process::Command;

#[cfg(target_os = "linux")]
pub fn install_href_router() {
    let mut homedir = match dirs::home_dir() {
        Some(d) => d,
        None => panic!("Failed to find home dir"),
    };

    homedir.push(".local/share/applications/href-router.desktop");
    let p = match homedir.to_str() {
        Some(x) => x,
        None => panic!("Failed to find home dir"),
    };
    fs::write(
        p,
        indoc! {r#"
            [Desktop Entry]
            Version=1.0
            Type=Application
            Name=Href Router
            Comment=Open links in the correct profile
            Exec=href-router open %u
            MimeType=x-scheme-handler/http;x-scheme-handler/https;x-scheme-handler/chrome;application/xhtml+xml;text/html;
       "#},
    )
    .expect("Failed to write .desktop file");

    // TODO add below
    // echo 'x-scheme-handler/https=href-router.desktop' | tee -a ~/.local/share/applications/defaults.list
    // echo 'x-scheme-handler/http=href-router.desktop' | tee -a ~/.local/share/applications/defaults.list
    // update-desktop-database ~/.local/share/applications/
    Command::new("sh")
        .arg("-c")
        .arg("xdg-mime default href-router.desktop text/html")
        .output()
        .expect("failed to set default handler");
    Command::new("sh")
        .arg("-c")
        .arg("xdg-mime default href-router.desktop application/xhtml+xml")
        .output()
        .expect("failed to set default handler");
    Command::new("sh")
        .arg("-c")
        .arg("xdg-mime default href-router.desktop x-scheme-handler/http")
        .output()
        .expect("failed to set default handler");
    Command::new("sh")
        .arg("-c")
        .arg("xdg-mime default href-router.desktop x-scheme-handler/https")
        .output()
        .expect("failed to set default handler");
}

#[cfg(target_os = "macos")]
pub fn install_href_router() {
    panic!("Not implmented for macos yet!")
}
