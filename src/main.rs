extern crate confy;
extern crate regex;
use chrono::{DateTime, Datelike, Local, NaiveTime};
use clap::{crate_name, ArgMatches};
use regex::RegexSet;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::fs;
use std::process::Command;
extern crate pretty_env_logger;
#[macro_use]
extern crate log;

mod cli;
mod installer;
mod logger;
mod validator;

#[derive(Default, Hash, PartialEq, Eq, Debug, Serialize, Deserialize, Clone)]
struct TodDuration {
    only_weekdays: bool,
    start_hour: u32,
    end_hour: u32,
}

impl std::fmt::Display for TodDuration {
    // noidea what I'm doing here
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "")
    }
}

type UrlBasedRoutingRules = HashMap<String, Vec<String>>;
type TimeBasedRoutingRules = HashMap<String, TodDuration>;

// profiles should have a static list of urls, time based routing rules dictate which list should be checked first (and first is the fallback)
#[derive(Default, Debug, Serialize, Deserialize, Clone)]
struct HrefRouterConfig {
    browser_path: String,
    browser_config_dir: String,
    browser_extra_flags: String,
    routing_rules: UrlBasedRoutingRules,
    time_based_routing_rules: TimeBasedRoutingRules,
    log_stdout: bool,
    open_debug_port: bool,
}

fn determine_profile_friendly_name_for_link(link: &str, config: UrlBasedRoutingRules) -> String {
    for (prof_name, list) in config.iter() {
        if let Ok(rgxset) = RegexSet::new(list) {
            if rgxset.is_match(link) {
                return prof_name.to_string();
            }
        }
    }
    "".to_string()
}

type LocalDate = DateTime<Local>;
type ParsedTimeRule = (LocalDate, LocalDate, bool, String);
type ParsedTimeRules = Vec<ParsedTimeRule>;

fn parse_tod_rules(dt_now: LocalDate, rules: TimeBasedRoutingRules) -> ParsedTimeRules {
    let today = dt_now.date();
    rules
        .iter()
        .map(|(prof_name, time_frame)| {
            let start_time = today
                .and_time(NaiveTime::from_hms(time_frame.start_hour, 0, 0))
                .unwrap();
            let end_time = today
                .and_time(NaiveTime::from_hms(time_frame.end_hour, 0, 0))
                .unwrap();
            (
                start_time,
                end_time,
                time_frame.only_weekdays,
                prof_name.to_string(),
            )
        })
        .collect()
}

fn determine_profile_friendly_name_for_time_of_day(config: HrefRouterConfig) -> String {
    let time_based_routing_rules: TimeBasedRoutingRules = config.time_based_routing_rules;
    let now: LocalDate = Local::now();
    let parsed_rules = parse_tod_rules(now, time_based_routing_rules);
    for (start_time, end_time, only_weekdays, prof_name) in parsed_rules.iter() {
        if only_weekdays.to_owned() && (now.weekday().number_from_monday() > 5) {
            continue;
        }
        if &now > start_time && &now < end_time {
            return prof_name.to_string();
        }
    }
    "".to_string()
}
#[derive(Serialize, Deserialize)]
struct ChromeProfileInfoCache {
    name: String,
}
#[derive(Serialize, Deserialize)]
struct ChromeLocalInfoCache {
    info_cache: HashMap<String, ChromeProfileInfoCache>,
}
#[derive(Serialize, Deserialize)]
struct ChromeLocalState {
    profile: ChromeLocalInfoCache,
}

fn get_internal_profile_names(browser_config_dir: &str) -> HashMap<String, String> {
    if browser_config_dir.is_empty() {
        warn!("No browser_config_dir is set in your configuration, so default browser will always open");
        return HashMap::new();
    }
    let chrome_local_state_str = fs::read_to_string(format!("{}/Local State", browser_config_dir))
        .expect("Unable to read chrome local state config");
    let chrome_local_state: ChromeLocalState =
        serde_json::from_str(&chrome_local_state_str).unwrap();

    let mut map = HashMap::new();
    for (name, info) in chrome_local_state.profile.info_cache.into_iter() {
        map.insert(info.name, name);
    }
    map.clone()
}

fn get_internal_name_from_friendly_name(friendly_name: &str, browser_config_dir: &str) -> String {
    let map = get_internal_profile_names(browser_config_dir);

    match map.get(friendly_name) {
        Some(name) => name.to_string(),
        None => "Default".to_string(),
    }
}

fn determine_profile_for_link(opts: ArgMatches, config: HrefRouterConfig) -> String {
    let link = opts.value_of("link").unwrap_or("");
    let is_day_off = opts.occurrences_of("personal") != 0;
    let config_param = config.clone();
    let routing_config = config.routing_rules;
    let mut friendly_name = determine_profile_friendly_name_for_link(link, routing_config);
    if friendly_name.is_empty() && !is_day_off {
        friendly_name = determine_profile_friendly_name_for_time_of_day(config_param)
    }
    get_internal_name_from_friendly_name(&friendly_name, &config.browser_config_dir)
}

fn load_config() -> Result<HrefRouterConfig, ::std::io::Error> {
    confy::load(crate_name!())
}

fn create_blank_config() -> Result<(), ::std::io::Error> {
    confy::store(
        crate_name!(),
        HrefRouterConfig {
            browser_path: "".to_string(),
            browser_config_dir: "".to_string(),
            browser_extra_flags: "".to_string(),
            log_stdout: false,
            open_debug_port: false,
            time_based_routing_rules: HashMap::new(),
            routing_rules: HashMap::new(),
        },
    )
}

fn return_ssh_client_hostname() -> Option<String> {
    match std::env::var("SSH_CLIENT") {
        Ok(v) => Some(v),
        Err(_) => None,
    }
}

fn app(opts: ArgMatches) {
    let link = opts.value_of("link").unwrap_or("");

    if let Some(homehost) = return_ssh_client_hostname() {
        let child = Command::new("ssh")
            .arg(homehost)
            .arg(format!("open {}", link))
            .spawn()
            .expect("return to sender failed to spawn cmd");
        child.wait().expect("return to sender failed!");
        std::process::exit(0);
    }

    let user_config = load_config().unwrap();
    let browser_path = user_config.browser_path.to_string();
    let target_profile = determine_profile_for_link(opts.clone(), user_config.clone());

    let extra_user_browser_flags = user_config.browser_extra_flags.to_string();
    let mut extra_browser_flags = vec![format!("{}", extra_user_browser_flags)];
    if user_config.open_debug_port {
        let remote_debugging_port = determine_debugging_port(&target_profile);
        debug!("using debug port: `{}`", remote_debugging_port);
        extra_browser_flags.push(format!("--remote-debugging-port={}", remote_debugging_port));
    }
    if browser_path.is_empty() {
        error!("No browser path provided, please edit config");
        std::process::exit(1);
    }
    info!(
        "Attempting to open '{}' with profile: {}",
        link, target_profile
    );
    let cmd = format!(
        "{browser_path} '{href}' --profile-directory=\"{target_profile}\" {flags}",
        href = link,
        target_profile = target_profile,
        browser_path = browser_path,
        flags = extra_browser_flags.join(" ")
    );
    debug!("shell command is: `{}`", cmd);
    let response = Command::new("sh")
        .arg("-c")
        .arg(cmd)
        .output()
        .expect("failed to open link in browser");
    if !response.status.success() {
        error!(
            "Failed to open browser with profile: {:?}",
            String::from_utf8(response.stderr)
        );
        std::process::exit(1);
    }
}

fn determine_debugging_port(target_profile: &str) -> i32 {
    let remote_debugging_port_offset = match target_profile {
        "Default" => 0,
        "cbi" => 1,
        "Pitch" => 2,
        _ => -1,
    };
    9222 + remote_debugging_port_offset
}

fn should_log_to_stdout() -> Result<bool, ::std::io::Error> {
    let user_config = load_config()?;
    return Ok(user_config.log_stdout || std::env::var("HREF_ROUTER_DEBUG").is_ok());
}

fn main() {
    if std::env::var("RUST_LIB_BACKTRACE").is_err() {
        std::env::set_var("RUST_LIB_BACKTRACE", "1")
    }
    if std::env::var("RUST_LOG").is_err() {
        std::env::set_var("RUST_LOG", "info")
    }
    color_eyre::install().unwrap();
    let log_to_stdout = should_log_to_stdout().unwrap_or(false);
    logger::create(log_to_stdout);
    info!("starting");
    let opts = cli::new().get_matches();
    info!("examining opts");
    if opts.is_present("install") {
        info!("installing");
        create_blank_config().unwrap();
        installer::install_href_router()
    } else if opts.is_present("validate") {
        info!("validating");
        let is_valid = validator::validate_all(opts);
        if !is_valid {
            std::process::exit(1);
        }
    } else if let Some(subcmd_opts) = opts.subcommand_matches("open") {
        info!("opening");
        // TODO check if --personal flag bubbles through subcmd opts
        app(subcmd_opts.clone())
    } else if let Some(subcmd_opts) = opts.subcommand_matches("check") {
        info!("checking");
        println!(
            "{:?}",
            determine_profile_for_link(subcmd_opts.clone(), load_config().unwrap())
        )
    } else if let Some(subcmd_opts) = opts.subcommand_matches("ping") {
        println!("pong")
    } else {
        info!("fallback case hit");
        app(opts)
    }
}
