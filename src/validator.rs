use chrono::Local;
use std::process::Command;
use std::str;

fn validate_install(_opts: super::ArgMatches) -> bool {
    let response = Command::new("sh")
        .arg("-c")
        .arg("xdg-settings get default-url-scheme-handler https")
        .output()
        .expect("xdg-settings check failed");
    if !response.status.success() {
        error!(
            "xdg-settings check failed\n{:?}",
            String::from_utf8(response.stderr)
        );
        std::process::exit(1);
    }

    let defaulthttps = match str::from_utf8(&response.stdout) {
        Ok(v) => v,
        Err(_) => "",
    };
    let response2 = Command::new("sh")
        .arg("-c")
        .arg("xdg-settings get default-url-scheme-handler https")
        .output()
        .expect("xdg-settings check failed");
    if !response2.status.success() {
        error!(
            "xdg-settings check failed\n{:?}",
            String::from_utf8(response2.stderr)
        );
        std::process::exit(1);
    }

    let defaulthttp = match str::from_utf8(&response2.stdout) {
        Ok(v) => v,
        Err(_) => "",
    };

    defaulthttps.contains(&String::from("href-router"))
        && defaulthttp.contains(&String::from("href-router"))
}

fn validate_conf(_opts: super::ArgMatches) -> bool {
    let mut valid = true;
    let user_config = super::load_config().unwrap();
    let name_mapping = super::get_internal_profile_names(&user_config.browser_config_dir);
    let browser_path = user_config.browser_path.to_string();

    let mut known_profile_names: Vec<String> = name_mapping.keys().cloned().collect();
    known_profile_names.push(String::from("default"));

    if browser_path.is_empty() {
        error!("No browser path provided, please edit config");
        std::process::exit(1);
    }

    let response = Command::new("sh")
        .arg("-c")
        .arg(format!(
            "{browser_path} --version",
            browser_path = browser_path
        ))
        .output()
        .expect("Invalid `browser_path` specified");
    if !response.status.success() {
        error!(
            "Checking browser version failed... is the configured path correct? {:?}",
            String::from_utf8(response.stderr)
        );
        std::process::exit(1);
    }

    let now: super::LocalDate = Local::now();
    super::parse_tod_rules(now, user_config.time_based_routing_rules);
    let unknown_profiles: Vec<_> = user_config
        .routing_rules
        .iter()
        .filter(|(prof, _)| !known_profile_names.contains(prof.to_owned()))
        .map(|(prof, _)| prof)
        .collect();
    if !unknown_profiles.is_empty() {
        valid = false;
        warn!(
            "Found unkown profiles in config: {:?}\nProfiles known to Chrome: {:?}",
            unknown_profiles, known_profile_names
        );
    }
    valid
}

pub fn validate_all(opts: super::ArgMatches) -> bool {
    validate_conf(opts.clone()) && validate_install(opts.clone())
}
